from flask import Flask, request, make_response, render_template
from flask.ext.bootstrap import Bootstrap
from flask.ext.script import Manager
from random import choice
from string import lowercase
import codecs
import base64

app = Flask(__name__)
bootstrap = Bootstrap(app)
manager = Manager(app)
app.config["DEBUG"] = True


def rand_string(n):
    return "".join(choice(lowercase) for i in xrange(n))

list_of_cookies = list()
for i in xrange(0, 100):
    string1 = rand_string(10)
    string2 = rand_string(13)
    list_of_cookies.append([string1, string2])


@app.route('/', methods=["GET", "POST"])
def index():
    name = base64.b64encode(codecs.encode("Flag:mavensec1", "rot_13"))
    if request.cookies.get('required') == 'True':
        return render_template('index.html', name=name)
    resp = make_response(render_template('index.html'))
    resp.set_cookie('required', 'False')
    for cookie in list_of_cookies:
        resp.set_cookie(cookie[0], cookie[1])
    return resp

if __name__ == '__main__':
    manager.run()
